<?php

/**
 * @file
 * Admin pages for entity_attachment.module.
 */

/**
 * Overview of entity_attachment types.
 */
function entity_attachment_type_overview() {
  $header = array(
    t('Entity attachment type'),
    array('data' => t('Operations'), 'colspan' => '2'),
  );
  $rows = array();

  foreach (entity_attachment_get_types() as $entity_attachment_type => $info) {
    $row = array();
    $row[] = array('data' => $info['label']);
    $row[] = array('data' => l(t('manage fields'), 'admin/structure/entity_attachment_types/' . $entity_attachment_type . '/fields'));
    $row[] = array('data' => l(t('manage display'), 'admin/structure/entity_attachment_types/' . $entity_attachment_type . '/display'));
    $rows[] = $row;
  }

  $output = theme('table', array('header' => $header, 'rows' => $rows));
  return $output;
}
