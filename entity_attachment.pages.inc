<?php

/**
 * @file
 * Page callbacks for entity attachments.
 */

/**
 * Page view for entity attachments.
 */
function entity_attachment_view($node, $entity_attachment) {
  if ($entity_attachment) {
    // Let entity attachment handle title.
    drupal_set_title($entity_attachment->getTitle(), PASS_THROUGH);
    // Allow other modules an opportunity to affect attachment displays

    return $entity_attachment->view('default', NULL, 1);
  }
  return MENU_NOT_FOUND;
}

/**
 * Entity attachment management form for entity_attachment node.
 */
function entity_attachment_manage($form, &$form_state, $node) {
  if (!isset($form['#entity_attachment_node'])) {
    $form['#entity_attachment_node'] = $node;
  }
  $label = variable_get('entity_attachment_label_' . $form['#entity_attachment_node']->type, 'attachment');
  drupal_set_title(ucfirst($label) . 's');


  if (isset($form_state['add'])) {
    // Show type selection form if there are more then one type.
    $types = entity_attachment_get_types($form['#entity_attachment_node']);
    $options = array();
    foreach ($types as $key => $type) {
      $options[$key] = $type['label'];
    }
    $form['type'] = array(
      '#type' => 'select',
      '#title' => t('Select attachment type'),
      '#options' => $options,
      '#description' => t('Select the type of !label to add', array('!label' => $label)),
    );
    $form['next'] = array(
      '#type' => 'submit',
      '#value' => t('Add !label', array('!label' => $label)),
      '#submit' => array('entity_attachment_manage_add'),
    );
  }
  else {
    $form['add'] = array(
      '#type' => 'submit',
      '#value' => t('Add !label', array('!label' => $label)),
      '#submit' => array('entity_attachment_manage_next'),
    );

    $entity_attachments = entity_attachment_load_node_entity_attachments($node);
    if ($entity_attachments) {
      $form['attachments']['#theme'] = 'entity_attachment_manage_table';
      $form['attachments']['#tree'] = TRUE;
      $order = 0;
      foreach ($entity_attachments as $key => $attachment) {
        $form['attachments'][$key]['#attachment'] = $attachment;
        $form['attachments'][$key]['#weight'] = $order++;
        $form['attachments'][$key]['weight'] = array(
          '#type' => 'textfield',
          '#title' => t('Weight for @title', array('@title' => $attachment->title)),
          '#title_display' => 'invisible',
          '#size' => 4,
          '#default_value' => $order,
          '#attributes' => array('class' => array('attachment-weight')),
        );
      }
      $form['attachments']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save'),
        '#submit' => array('entity_attachment_manage_save_weight'),
      );
    }
  }

  return $form;
}

/**
 * Submit callback from entity attachment management form.
 *
 * Advances to the next page to select type or redirect to add attachment form.
 */
function entity_attachment_manage_next($form, &$form_state) {
  $types = entity_attachment_get_types($form['#entity_attachment_node']);

  // Show the type selection form if there is more than one type, otherwise
  // redirect to attachment add form for the only type.
  if (count($types) > 1) {
    $form_state['add'] = TRUE;
    $form_state['rebuild'] = TRUE;
  }
  else {
    $type = current($types);
    $form_state['redirect'] = 'node/' . $form['#entity_attachment_node']->nid . '/manage-attachments/' . $type['type'];

    // GET destination needs to be unset, otherwise it will override the form
    // state redirect.
    unset($_GET['destination']);
  }
}

/**
 * Submit callback from entity attachment management form, returns the add an attachment
 * form.
 */
function entity_attachment_manage_add($form, &$form_state) {
  // Redirect to entity attachment add form.
  $form_state['redirect'] = 'node/' . $form['#entity_attachment_node']->nid . '/manage-attachments/' . $form_state['values']['type'];
}

/**
 * Submit callback from entity attachment management form, saves weights.
 */
function entity_attachment_manage_save_weight($form, &$form_state) {
  foreach ($form_state['values']['attachments'] as $sid => $values) {
    if (is_int($sid)) {
      $attachment = $form['attachments'][$sid]['#attachment'];
      $attachment->weight = $values['weight'];
      $attachment->save();
    }
  }
  $label = variable_get('entity_attachment_label_' . $form['#entity_attachment_node']->type, 'attachment');
  drupal_set_message(t('!label weights updated.'), array('!label' => ucfirst($label)));
}

/**
 * Returns HTML for the entity attachment order table.
 */
function theme_entity_attachment_manage_table($variables) {
  $form = $variables['form'];

  $header = array(t('Title'), t('Type'), t('Weight'), array('data' => t('Operations'), 'colspan' => 2));
  foreach (element_children($form) as $key) {
    if (isset($form[$key]['#attachment'])) {
      $attachment = $form[$key]['#attachment'];
      $row = array();
      $row[] = check_plain($attachment->title);
      $row[] = check_plain($attachment->type);
      $row[] = drupal_render($form[$key]['weight']);
      $row[] = l(t('edit'), $attachment->editURL()) .  ' ' . l(t('clone'), $attachment->cloneURL()) . ' ' . l(t('delete'), $attachment->deleteURL());
      $rows[] = array('data' => $row, 'class' => array('draggable'));
    }
  }

  drupal_add_tabledrag('entity-attachments', 'order', 'sibling', 'attachment-weight');

  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'entity-attachments')));
  $output .= drupal_render_children($form);

  return $output;
}

/**
 * Edit entity attachment page callback.
 */
function entity_attachment_edit($node, $entity_attachment) {
  if ($entity_attachment) {
    return drupal_get_form('entity_attachment_form', $entity_attachment);
  }
  return MENU_NOT_FOUND;
}

/**
 * Add entity attachment page callback.
 */
function entity_attachment_add($node, $entity_attachment_type) {
  $entity_attachment = entity_attachment_create(array('nid' => $node->nid, 'type' => $entity_attachment_type));

  return drupal_get_form('entity_attachment_form', $entity_attachment, $entity_attachment_type);
}

/**
 * Entity attachment add/edit form.
 */
function entity_attachment_form($form, &$form_state, EntityAttachment $entity_attachment, $entity_attachment_type = NULL) {

  // During initial form build, add the entity_attachment entity to the form state for use
  // during form building and processing. During a rebuild, use what is in the
  // form state.
  if (!isset($form_state['entity_attachment'])) {
    if (isset($entity_attachment_type)) {
      $entity_attachment->type = $entity_attachment_type;
    }
    $form_state['entity_attachment'] = $entity_attachment;
  }
  else {
    $entity_attachment = $form_state['entity_attachment'];
  }

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('The title of this %entity_attachment_type', array('%entity_attachment_type' => $entity_attachment->type)),
    '#default_value' => $entity_attachment->title,
    '#weight' => -9,
    '#required' => TRUE,
  );

  $form['entity_attachment'] = array(
    '#type' => 'value',
    '#value' => $entity_attachment,
  );

  // Add the buttons.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 200,
    '#validate' => array('entity_attachment_form_validate'),
    '#submit' => array('entity_attachment_form_submit'),
  );
  //TODO I dont think this permission exists
  if (!empty($entity_attachment->sid) && user_access('delete any ' . $entity_attachment->type . ' entity_attachment')) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#weight' => 201,
      '#submit' => array('entity_attachment_form_delete_submit'),
    );
  }

  if (!isset($form['#submit']) && function_exists($entity_attachment->type . '_entity_attachment_form_submit')) {
    $form['#submit'][] = $entity_attachment->type . '_entity_attachment_form_submit';
  }
  $form += array('#submit' => array());

  field_attach_form('entity_attachment', $entity_attachment, $form, $form_state);
  return $form;
}

/**
 * Validation for entity attachment form
 */
function entity_attachment_form_validate($form, &$form_state) {
  $entity_attachment = $form_state['values']['entity_attachment'];
  $entity_attachment->title = $form_state['values']['title'];

  field_attach_form_validate('entity_attachment', $entity_attachment, $form, $form_state);
  $form_state['values']['entity_attachment'] = $entity_attachment;
}

/**
 * Submit function for entity attachment form
 */
function entity_attachment_form_submit($form, &$form_state) {
  $entity_attachment = entity_attachment_form_submit_build_entity_attachment($form, $form_state);
  $update = $entity_attachment->internalIdentifier();
  field_attach_submit('entity_attachment', $entity_attachment, $form, $form_state);
  $entity_attachment->save();

  $watchdog_args = array('@type' => $entity_attachment->type, '%title' => $entity_attachment->title);
  $t_args = array('@type' => $entity_attachment->type, '%title' => $entity_attachment->title);

  if ($update) {
    watchdog('entity_attachment', '@type: updated %title.', $watchdog_args, WATCHDOG_NOTICE, $entity_attachment->viewURL());
    drupal_set_message(t('@type %title has been updated.', $t_args));
  }
  else {
    watchdog('entity_attachment', '@type: added %title.', $watchdog_args, WATCHDOG_NOTICE, $entity_attachment->viewURL());
    drupal_set_message(t('@type %title has been created.', $t_args));
  }

  if ($entity_attachment->identifier()) {
    $form_state['redirect'] = $entity_attachment->viewURL();
  }
  else {
    // In the unlikely case something went wrong on save, the entity_attachment will be
    // rebuilt and entity attachment form redisplayed the same way as in preview.
    drupal_set_message(t('The entity attachment could not be saved.'), 'error');
    $form_state['rebuild'] = TRUE;
  }

  // Clear page and block caches.
  cache_clear_all();
}

/**
 * Updates the form state's entity_attachment entity by processing this submission's values.
 *
 * This is the default builder function for the entity_attachment form. It is called
 * during the "Save" and "Preview" submit handlers to retrieve the entity to
 * save or preview. This function can also be called by a "Next" button of a
 * wizard to update the form state's entity with the current step's values
 * before proceeding to the next step.
 *
 * @see entity_attachment_form()
 * @see node_form_submit_build_node()
 */
function entity_attachment_form_submit_build_entity_attachment($form, &$form_state) {

  // @todo Legacy support for modules that extend the entity_attachment form with form-level
  //   submit handlers that adjust $form_state['values'] prior to those values
  //   being used to update the entity. Module authors are encouraged to instead
  //   adjust the entity_attachment directly within a hook_entity_attachment_submit() implementation. For
  //   Drupal 8, evaluate whether the pattern of triggering form-level submit
  //   handlers during button-level submit processing is worth supporting
  //   properly, and if so, add a Form API function for doing so.
  unset($form_state['submit_handlers']);
  form_execute_handlers('submit', $form, $form_state);

  $entity_attachment = $form_state['entity_attachment'];
  entity_form_submit_build_entity('entity_attachment', $entity_attachment, $form, $form_state);

  foreach (module_implements('entity_attachment_submit') as $module) {
    $function = $module . '_entity_attachment_submit';
    $function($entity_attachment, $form, $form_state);
  }
  return $entity_attachment;
}

/**
 * Button submit function: handle the 'Delete' button on the entity attachment
 * form.
 */
function entity_attachment_form_delete_submit($form, &$form_state) {
  $destination = array();
  if (isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }
  $entity_attachment = $form_state['values']['entity_attachment'];
  $form_state['redirect'] = array($entity_attachment->deleteURL(), array('query' => $destination));
}

/**
 * Menu callback -- ask for confirmation of entity attachment deletion.
 */
function entity_attachment_delete_confirm($form, &$form_state, $node, $entity_attachment) {
  $form['#entity_attachment'] = $entity_attachment;

  return confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => $entity_attachment->title)),
    $entity_attachment->viewURL(),
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Execute entity attachment deletion.
 */
function entity_attachment_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $entity_attachment = $form['#entity_attachment'];
    $entity_attachment->delete();
    watchdog('entity_attachment', '@type: deleted %title.', array('@type' => $entity_attachment->type, '%title' => $entity_attachment->title));
    drupal_set_message(t('@type %title has been deleted.', array('@type' => $entity_attachment->type, '%title' => $entity_attachment->title)));
  }

  $form_state['redirect'] = 'node/' . $entity_attachment->nid;
}

/**
 * Menu callback -- clone current entity attachment in the current node.
 */
function entity_attachment_clone($node, $entity_attachment) {
  unset($entity_attachment->eid);
  unset($entity_attachment->delta);
  return drupal_get_form('entity_attachment_form', $entity_attachment);
}
