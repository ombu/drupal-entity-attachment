<?php

/**
 * @file
 */

/**
 * Default EntityAttachmentType class.
 */
abstract class EntityAttachmentType implements EntityAttachmentTypeInterface {
  /**
   * The entity attachment object being viewed.
   *
   * @var EntityAttachment
   */
  protected $entity_attachment;

  /**
   * The info related to this entity attachment type.
   *
   * @var array
   */
  protected $entity_attachment_info;

  /**
   * Constructor.
   */
  public function __construct($entity_attachment_info) {
    $this->entity_attachment_info  = $entity_attachment_info;
  }

  /**
   * Get Plugin info.
   */
  public function getInfo($key = NULL) {
    if (!empty($key) && isset($this->entity_attachment_info[$key])) {
      return $this->entity_attachment_info[$key];
    }

    return $this->entity_attachment_info;
  }

  /**
   * Build the URL string for the entity attachment.
   */
  public function buildURL() {
    return str_replace('_', '-', $this->type);
  }

  /**
   * Get the label name of the entity attachment.
   *
   * @abstract
   *
   * @return string
   *   Label of the type.
   */
  public function getLabel() {
    return $this->type;
  }

  /**
   * Returns page title for the entity attachment.
   */
  public function getTitle() {
    $node = node_load($this->entity_attachment->nid);
    return $node->title . ' <small>' . $this->entity_attachment->title . '</small>';
  }

  /**
   * Set the EntityAttachment object for later use.
   *
   * @abstract
   *
   * @param EntityAttachment $entity_attachment
   */
  public function setEntityAttachment(EntityAttachment $entity_attachment) {
    $this->entity_attachment = $entity_attachment;
  }

  /**
   * Return page content.
   *
   * @abstract
   *
   * @param EntityAttachment $entity_attachment
   *   The entity attachment object being viewed.
   * @param array $content
   *   The default content array created by Entity API.  This will include any
   *   fields attached to the entity.
   * @param string $view_mode
   *   The view mode passed into $entity->view().
   *
   * @return array
   *   Return a renderable content array.
   */
  public function view(EntityAttachment $entity_attachment, $content, $view_mode = 'default', $langcode = NULL) {
    return $content;
  }
}
