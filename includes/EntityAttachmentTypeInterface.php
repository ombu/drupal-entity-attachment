<?php

/**
 * @file
 */

/**
 * Interface for Entity Attachment Type classes.
 */
interface EntityAttachmentTypeInterface {
  /**
   * Constructor.
   *
   * @abstract
   *
   * @param array $entity_attachment_info
   *   Array of information from the ctools plugin.
   */
  public function __construct($entity_attachment_info);

  /**
   * Build the URL string for the entity attachment.
   *
   * @abstract
   *
   * @return string
   *   Url to entity attachment.
   */
  public function buildURL();

  /**
   * Get the label name of the entity attachment type.
   *
   * @abstract
   *
   * @return string
   *   Label of the type
   */
  public function getLabel();

  /**
   * Set the EntityAttachment object for later use.
   *
   * @abstract
   *
   * @param EntityAttachment $entity_attachment
   *   The entity attachment being operated on.
   */
  public function setEntityAttachment(EntityAttachment $entity_attachment);

  /**
   * Return page content.
   *
   * @abstract
   *
   * @param EntityAttachment $entity_attachment
   *   The entity attachment object being viewed.
   * @param array $content
   *   The default content array created by Entity API.  This will include any
   *   fields attached to the entity.
   * @param string $view_mode
   *   The view mode passed into $entity->view().
   *
   * @return array
   *   Return a renderable content array.
   */
  public function view(EntityAttachment $entity_attachment, $content, $view_mode = 'default', $langcode = NULL);
}
