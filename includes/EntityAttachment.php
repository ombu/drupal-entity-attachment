<?php

/**
 * @file
 */

/**
 * The EntityAttachment entity class.
 */
class EntityAttachment extends Entity {
  /**
   * Admin label for this EntityAttachment.
   *
   * @var string
   */
  public $label;

  /**
   * Display title for this EntityAttachment.
   *
   * @var string
   */
  public $title;

  /**
   * Raw type of this EntityAttachment.
   *
   * @var string
   */
  public $type;

  /**
   * EntityAttachmentType for this EntityAttachment.
   *
   * @var EntityAttachmentType
   */
  protected $entity_attachment_type;

  /**
   * Get Entity  info
   */
  public function getInfo($key = NULL) {
    if ($this->entity_attacment_type instanceof EntityAttachmentTypeInterface) {
      return $this->entity_attacment_type->getInfo($key);
    }

    return NULL;
  }

  public function __construct(array $values = array()) {
    // If this is new then set the type first
    if (isset($values['is_new'])) {
      $this->type = isset($values['type']) ? $values['type'] : '';
    }

    parent::__construct($values, 'entity_attachment');
  }

  protected function setUp() {
    parent::setUp();

    if (!empty($this->type)) {
      $entity_attachment_type = entity_attachment_load_class($this->type);
      $this->loadUp($entity_attachment_type);
    }
  }

  /**
   * This is a work around for version of PDO that call __construct() before
   * it loads up the object with values from the DB.
   */
  public function loadUp(EntityAttachmentTypeInterface $entity_attachment_type) {
    if (empty($this->entity_attachment_type)) {
      $this->setEntityAttachmentType($entity_attachment_type);
    }
  }

  /**
   * Load and set the entity attachment type info.
   *
   * This can be called externally via loadUP()
   */
  protected function setEntityAttachmentType(EntityAttachmentTypeInterface $entity_attachment_type) {
    $this->entity_attachment_type = $entity_attachment_type;
    $this->entity_attachment_type->setEntityAttachment($this);
  }

  /**
   * Override this in order to implement a custom default URI and specify
   * 'entity_class_uri' as 'uri callback' hook_entity_info().
   */
  protected function defaultUri() {
    return array('path' => 'node/' . $this->nid . '/' . $this->type . '/' . $this->delta);
  }

  /**
   * URL string
   */
  public function base() {
    return 'node/' . $this->nid;
  }

  /**
   * URL string
   */
  public function delta() {
    return '/' . $this->type . '/' . $this->delta;
  }

  /**
   * URL string
   */
  public function url() {
    $uri = $this->defaultUri();
    return $uri['path'];
  }

  /**
   * Build the URL string
   */
  protected function buildURL($type = NULL) {
    $url = $this->url();
    return $url . '/' . $type;
  }

  /**
   * View URL
   */
  public function viewURL() {
    return $this->buildURL();
  }

  /**
   * Edit URL
   */
  public function editURL() {
    return $this->buildURL('edit');
  }

  /**
   * Delete URL
   */
  public function deleteURL() {
    return $this->buildURL('delete');
  }

  /**
   * Clone URL
   */
  public function cloneURL() {
    return $this->buildURL('clone');
  }

  /**
   * Returns page title for the entity attachment.
   */
  public function getTitle() {
    return $this->entity_attachment_type->getTitle();
  }

  /**
   * Returns the label for the entity attachment.
   */
  public function label() {
    $node = node_load($this->nid);
    return $node->title . ': ' . $this->title;
  }

  /**
   * Generate an array for rendering the entity.
   *
   * @see entity_view()
   */
  public function view($view_mode = 'default', $langcode = NULL, $page = NULL) {
    $content = parent::view($view_mode, $langcode, $page);

    return $this->entity_attachment_type->view($this, $content, $view_mode, $langcode);
  }

  /**
   * Override the save to set the delta.
   */
  public function save() {
    // Set the delta if it's not set already.
    if (empty($this->delta)) {
      $max_length = 32;

      // Base it on the label and make sure it isn't too long for the database.
      if (module_exists('transliteration')) {
        $delta = drupal_clean_css_identifier(transliteration_get($this->title));
      }
      else {
        $delta = drupal_clean_css_identifier(strtolower($this->title));;
      }

      $this->delta = substr($delta, 0, $max_length);

      // Check if delta is unique.
      if (entity_attachment_delta_load($this->delta, $this->nid)) {
        $i = 0;
        $separator = '-';
        $original_delta = $this->delta;
        do {
          $unique_suffix = $separator . $i++;
          $this->delta = substr($original_delta, 0, $max_length - drupal_strlen($unique_suffix)) . $unique_suffix;
        } while (entity_attachment_delta_load($this->delta, $this->nid));
      }
    }

    $return = parent::save();
    return $return;
  }

  /**
   * Override the delete to remove the fields and blocks.
   */
  public function delete() {
    if ($this->internalIdentifier()) {
      // Delete the field values
      field_attach_delete('entity_attachment', $this);
      entity_get_controller($this->entityType)->delete(array($this->internalIdentifier()));
    }
  }
}
